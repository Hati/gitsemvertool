

import subprocess
import os

Config = {
    'Prefix': 'SEMVER_',
    'Meta': 'meta-',
    'Version': 'v'
}

BUILD_TAG_QUERY = 'git describe --always --tags --abbrev=0 --match {Meta}*'.format(Meta=Config['Meta'])
VERSION_TAG_QUERY = 'git describe --always --tags --match {Version}*'.format(Version=Config['Version'])
BUILD_HASH_QUERY = 'git describe --always --abbrev=12 --exclude "*"'

# Versioning via 

# Get last tag-* that describes a version tag
# git describe --always --tags --abbrev=0 --match {Meta}*
# -> last Meta

# Get last version tag
# git describe --always --tags --match v*
# -> v{Major}.{Minor}.{Patch}-{buildmeta}

def GetBuildTag():
    BUILD_TAG_PROCESS = subprocess.run(BUILD_TAG_QUERY, text=True, capture_output=True)
    BUILD_TAG = BUILD_TAG_PROCESS.stdout[:len(Config['Meta'])] # Cut tag-
    if BUILD_TAG == Config['Meta']: 
        BUILD_TAG = BUILD_TAG_PROCESS.stdout[len(Config['Meta']):] # Cut tag-
        return BUILD_TAG[:-1], True # also remove the \n
    else:
        BUILD_TAG = ""
        return BUILD_TAG, False

def GetBuildHash():
    BUILD_HASH_PROCESS = subprocess.run(BUILD_HASH_QUERY, text=True, capture_output=True)
    return str(BUILD_HASH_PROCESS.stdout)[:-1]

def ProcessVersionTag():
    SEMVER_PROCESS = subprocess.run(VERSION_TAG_QUERY, text=True, capture_output=True)
    PROCESS_STDOUT = SEMVER_PROCESS.stdout
    if PROCESS_STDOUT[:1] == Config['Version']: # is valid version tag
        RawVersion = PROCESS_STDOUT[len(Config['Version']):]
        VersionArr = RawVersion.split('.')
        BUILD_META = RawVersion.split('-')
        if len(BUILD_META) > 1:
            VersionArr[2] = VersionArr[2].split('-')[0]
            BUILD_META = int(BUILD_META[1])
        else:
            BUILD_META = 0
        return {
            'Major': int(VersionArr[0]),
            'Minor': int(VersionArr[1]),
            'Patch': int(VersionArr[2]),
            'BUILDMETA': BUILD_META
        }
    else:
        return {
            'Major': 0,
            'Minor': 0,
            'Patch': 0,
            'BUILDMETA': 0
        }

if __name__ == '__main__':
    path = os.path.dirname(os.path.realpath(__file__))
    os.chdir(path)
    Semver = ProcessVersionTag()
    BUILD_TAG, UsesBuildTag = GetBuildTag()
    Hash = GetBuildHash()

    semverFileDirectory = path
    print(semverFileDirectory)
    semverFilePath = os.path.join(semverFileDirectory, 'SEMVER.h')


    Version = {
        **Semver,
        'METATAG': BUILD_TAG,
        'BUILD_HASH': Hash
    }

    if UsesBuildTag:
        Version['USE_TAG'] = 1
    else:
        Version['USE_TAG'] = 0

    SemverString = ''
    if UsesBuildTag:
        SemverString = '{Major}.{Minor}.{Patch}-{METATAG}+{BUILDMETA}'.format(**Version)
    else:
        SemverString = '{Major}.{Minor}.{Patch}+{BUILDMETA}'.format(**Version)

    if os.path.exists(semverFilePath):
        f = open(semverFilePath, 'r')
        firstLine = f.readline()
        if (SemverString + " " + Hash) in firstLine:
            print('No Semver Update required.')
            exit(0)
        f.close()


    if not os.path.exists(semverFileDirectory):
        os.makedirs(semverFileDirectory)
    print(Version)
    f = open(semverFilePath, 'w')
    f.write('// ' + SemverString + " " + Hash)
    f.write('\n')

    Version['FULL_STR'] = '{VersionPrefix}{Semver} ({Hash})'.format(VersionPrefix=Config['Version'], Semver=SemverString, Hash=Hash)

    for key,value in Version.items():
        if type(value) == str:
            f.write('#define {Prefix}{key} "{value}"'.format(**{'key':key, 'value':value}, Prefix=Config['Prefix']))
        else:
            f.write('#define {Prefix}{key} {value}'.format(**{'key':key, 'value':value}, Prefix=Config['Prefix']))
        f.write('\n')
    f.write('#define {Prefix}STR "{SemverString}"'.format(SemverString=SemverString, Prefix=Config['Prefix']))
    f.close()
    


